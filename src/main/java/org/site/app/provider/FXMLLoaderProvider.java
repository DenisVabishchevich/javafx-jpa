package org.site.app.provider;

import javafx.fxml.FXMLLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class FXMLLoaderProvider {

    private final AnnotationConfigApplicationContext context;

    @Autowired
    public FXMLLoaderProvider(AnnotationConfigApplicationContext context) {
        this.context = context;
    }

    public FXMLLoader getFXMLLoader(String path) {
        FXMLLoader loader = new FXMLLoader();
        loader.setControllerFactory(context::getBean);
        loader.setLocation(getClass().getClassLoader().getResource(path));
        return loader;

    }
}
