package org.site.app.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.site.app.domain.Person;
import org.site.app.domain.Skill;
import org.site.app.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class DialogWindowController {

    private final PersonService service;

    private String actionId;

    private Person person;

    private ObservableList<Person> oList;

    private TableView<Person> parentTableView;

    @FXML
    private TextField skills;

    @FXML
    private TextField lastName;

    @FXML
    private TextField name;

    @FXML
    private TextField exp;

    @FXML
    private TextField age;

    @Autowired
    public DialogWindowController(PersonService service) {
        this.service = service;
    }

    @FXML
    private void initialize() {
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public TableView<Person> getTableView() {
        return parentTableView;
    }

    public void setTableView(TableView<Person> tableView) {
        this.parentTableView = tableView;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        showPersonDetails(person);
        this.person = person;
    }

    public ObservableList<Person> getoList() {
        return oList;
    }

    public void setoList(ObservableList<Person> oList) {
        this.oList = oList;
    }

    private void showPersonDetails(Person selectedPerson) {
        if (selectedPerson != null) {
            name.setText(selectedPerson.getName());
            lastName.setText(selectedPerson.getLastName());
            age.setText("" + selectedPerson.getAge());
            exp.setText("" + selectedPerson.getExp());
            skills.setText(selectedPerson.getSkills().stream().map((Skill v) -> v.getValue() + ",")
                    .collect(Collectors.joining())
            );
        } else {
            name.setText("");
            lastName.setText("");
            age.setText("");
            exp.setText("");
            skills.setText("");
        }

    }

    public void actionHide(ActionEvent actionEvent) {

        Node node = (Node) actionEvent.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.hide();
    }

    public void createOrUpdate(ActionEvent e) {

        if (actionId.equals("updateBtn")) {
            person.setName(name.getText());
            person.setLastName(lastName.getText());
            person.setAge(Integer.parseInt(age.getText()));
            List<String> list = new ArrayList(Arrays.asList(skills.getText().split(",")));
            person.setSkills(list.stream().map(elem -> new Skill(elem)).collect(Collectors.toSet()));
            person.setExp(Double.parseDouble(exp.getText()));
            service.updatePerson(person);
        } else if (actionId.equals("newBtn")) {
            
            Person newPerson = new Person();
            newPerson.setName(name.getText());
            newPerson.setLastName(lastName.getText());
            newPerson.setAge(Integer.parseInt(age.getText()));
            List<String> list = new ArrayList(Arrays.asList(skills.getText().split(",")));
            newPerson.setSkills(list.stream().map(elem -> new Skill(elem)).collect(Collectors.toSet()));
            newPerson.setExp(Double.parseDouble(exp.getText()));
            service.addPerson(newPerson);
            oList.add(newPerson);
        }

        parentTableView.refresh();
        actionHide(e);
    }
}
