package org.site.app.controller;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.site.app.domain.Person;
import org.site.app.domain.Skill;
import org.site.app.provider.FXMLLoaderProvider;
import org.site.app.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class MainWindowController {

    private final PersonService service;

    private final FXMLLoaderProvider provider;

    @FXML
    private Label count;

    @FXML
    private Button newBtn;

    @FXML
    private Label skills;

    @FXML
    private Label lastName;

    @FXML
    private Label name;

    @FXML
    private Label company;

    @FXML
    private Label exp;

    @FXML
    private Label age;

    @FXML
    private TableView<Person> table;

    @FXML
    private TableColumn<Person, String> nameColumn;

    @FXML
    private TableColumn<Person, String> lastNameColumn;

    private ObservableList<Person> oList;

    private DialogWindowController dialogWindowController;

    private Parent fxmlCreate;

    private Stage stage;

    @Autowired
    public MainWindowController(PersonService service, FXMLLoaderProvider provider) {
        this.service = service;
        this.provider = provider;
    }

    @FXML
    private void initialize() {
        fillTableWithData();
        initDialogWindow();
        addListenerForUpdateCount();
        updateCount();
    }

    private void addListenerForUpdateCount() {
        oList.addListener((ListChangeListener<? super Person>) event -> updateCount());
    }

    private void fillTableWithData() {
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        List<Person> persons = service.loadPersons();
        oList = FXCollections.observableArrayList(persons);
        table.setItems(oList);
    }

    private void initDialogWindow() {
        try {
            FXMLLoader loader = this.provider.getFXMLLoader("dialog_window.fxml");
            fxmlCreate = loader.load();
            dialogWindowController = loader.getController();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCount() {
        count.setText("Size:" + oList.size());
    }

    public void showDialogWindow(ActionEvent actionEvent) { // Edit.. or New button click event
        if (stage == null) {
            createDialogWindowStage();
            stage.initOwner(((Node) actionEvent.getSource()).getScene().getWindow());
        }
        Person p = table.getSelectionModel().getSelectedItem();
        dialogWindowController.setPerson(p);
        dialogWindowController.setoList(oList);
        dialogWindowController.setTableView(table);
        dialogWindowController.setActionId(((Button) actionEvent.getSource()).getId());
        stage.showAndWait();
        fillPersonDetails(p);
    }

    private void createDialogWindowStage() {
        try {
            stage = new Stage();
            stage.setTitle("Dialog window");
            stage.setResizable(false);
            Scene scene = new Scene(fxmlCreate, 445, 246);
            stage.setScene(scene);
            stage.initModality(Modality.WINDOW_MODAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deletePerson(ActionEvent e) {  // delete button click event
        Person selectedPerson = table.getSelectionModel().getSelectedItem();
        if (selectedPerson != null) {
            oList.remove(selectedPerson);
            service.removePerson(selectedPerson);
            fillPersonDetails(null);
        }
    }

    public void showDetailsOnForm(MouseEvent e) { // table single click on Person event
        Person selectedPerson = table.getSelectionModel().getSelectedItem();
        fillPersonDetails(selectedPerson);
    }

    private void fillPersonDetails(Person selectedPerson) {
        if (selectedPerson != null) {
            name.setText(selectedPerson.getName());
            lastName.setText(selectedPerson.getLastName());
            age.setText("" + selectedPerson.getAge());
            exp.setText("" + selectedPerson.getExp());
            skills.setText(selectedPerson.getSkills().stream().map((Skill v) -> v.getValue() + ",")
                    .collect(Collectors.joining())
            );
        } else {
            name.setText("");
            lastName.setText("");
            age.setText("");
            exp.setText("");
            skills.setText("");
        }
    }
}
