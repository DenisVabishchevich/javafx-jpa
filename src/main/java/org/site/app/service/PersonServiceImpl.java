package org.site.app.service;

import org.site.app.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    private final EntityManagerFactory emf;

    private EntityManager em;

    @Autowired
    public PersonServiceImpl(EntityManagerFactory emf) {
        this.emf = emf;
        em = emf.createEntityManager();
    }

    public List<Person> loadPersons() {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        List<Person> resultList = em.createQuery("select distinct p from Person p join fetch p.skills", Person.class).getResultList();
        tx.commit();
        return resultList;

    }

    @Override
    public Person addPerson(Person person) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(person);
        tx.commit();
        return person;
    }

    @Override
    public Person updatePerson(Person person) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.merge(person);
        tx.commit();
        return person;
    }

    public boolean removePerson(Person person) {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(person);
        tx.commit();
        return true;
    }

}
