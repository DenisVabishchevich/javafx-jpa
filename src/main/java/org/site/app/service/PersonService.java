package org.site.app.service;

import org.site.app.domain.Person;

import java.util.List;

public interface PersonService {

    List<Person> loadPersons();

    Person addPerson(Person person);

    Person updatePerson(Person person);

    boolean removePerson(Person person);

}
