package org.site.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.site.app.provider.FXMLLoaderProvider;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);
        primaryStage.setTitle("Книга учета");
        primaryStage.setMinHeight(600);
        primaryStage.setMinWidth(800);
        FXMLLoader loader = context.getBean(FXMLLoaderProvider.class).getFXMLLoader("main_window.fxml");
        Parent root = loader.load();
        Scene scene = new Scene(root, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();

        primaryStage.setOnCloseRequest(e -> context.close());

    }
}
