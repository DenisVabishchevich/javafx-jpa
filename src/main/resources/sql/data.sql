INSERT INTO SKILL VALUES (NULL, 'java');
INSERT INTO SKILL VALUES (NULL, 'C++');
INSERT INTO SKILL VALUES (NULL, 'Python');
INSERT INTO SKILL VALUES (NULL, 'SQL');

INSERT INTO PERSON (ID, AGE, EXP, LASTNAME, NAME) VALUES (NULL, 27, 1.3, 'Vabishchevich', 'Denis');
INSERT INTO PERSON (ID, AGE, EXP, LASTNAME, NAME) VALUES (NULL, 23, 1, 'Kuralena', 'Elena');
INSERT INTO PERSON (ID, AGE, EXP, LASTNAME, NAME) VALUES (NULL, 45, 13, 'Ivanov', 'Ivan');

INSERT INTO PERSON_SKILL (PERSON_ID, SKILL_ID) VALUES (1, 1);
INSERT INTO PERSON_SKILL (PERSON_ID, SKILL_ID) VALUES (1, 2);
INSERT INTO PERSON_SKILL (PERSON_ID, SKILL_ID) VALUES (2, 3);
INSERT INTO PERSON_SKILL (PERSON_ID, SKILL_ID) VALUES (3, 2);
INSERT INTO PERSON_SKILL (PERSON_ID, SKILL_ID) VALUES (3, 3);
INSERT INTO PERSON_SKILL (PERSON_ID, SKILL_ID) VALUES (3, 4);
INSERT INTO PERSON_SKILL (PERSON_ID, SKILL_ID) VALUES (3, 1);
