CREATE TABLE PERSON (
  ID           BIGINT      NOT NULL GENERATED ALWAYS AS IDENTITY,
  CREATIONDATE TIMESTAMP,
  NAME         VARCHAR(15) NOT NULL,
  LASTNAME     VARCHAR(15) NOT NULL,
  AGE          INTEGER     NOT NULL,
  EXP          INTEGER,
  PRIMARY KEY (ID)
);
CREATE TABLE SKILL (
  ID    BIGINT      NOT NULL GENERATED ALWAYS AS IDENTITY,
  VALUE VARCHAR(15) NOT NULL,
  PRIMARY KEY (ID)
);
CREATE TABLE PERSON_SKILL (
  PERSON_ID BIGINT,
  SKILL_ID  BIGINT,
  PRIMARY KEY (PERSON_ID, SKILL_ID)
);



